import React, { Component } from 'react';
import Number from './Number';
import './App.css';

class App extends Component {
    newNumbers = () => {
        const numbers = [];
        let i = 5;
        while(i > 0){
            const randomNum = Math.floor(Math.random() * 36 + 1);
            if(numbers.indexOf(randomNum) === -1 && randomNum >= 5) {
                numbers.push(randomNum);
                i--;
            }
        }
        numbers.sort((a, b) => a - b);
        this.setState({numbers});
    };

    state = {
        numbers: [],
        isLoading: false
    };

    componentDidMount(){
        this.newNumbers();
    }

    render() {
        return (
          <div className="container">
              <button onClick={this.newNumbers}>New numbers</button>
              <div className="numbersWrapper">
                  {this.state.numbers.map((number, i) => <Number key={i} randomNumber={number}/>)}
              </div>
          </div>
        );
    }
    }

export default App;
